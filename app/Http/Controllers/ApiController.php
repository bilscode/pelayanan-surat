<?php

namespace App\Http\Controllers;

use App\Http\Requests\CertificateRequest;
use App\Http\Requests\CoverLetterRequest;
use App\Http\Requests\PublicComplaintRequest;
use App\Models\Certificate;
use App\Models\CoverLetter;
use App\Models\PublicComplaint;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public $public_complaint, $certificate, $cover_letter;

    public function __construct()
    {
        $this->public_complaint = new PublicComplaint();
        $this->certificate = new Certificate();
        $this->cover_letter = new CoverLetter();
    }

    public function publicComplaint(PublicComplaintRequest $request)
    {
        return response()->json([
            'status' => true,
            'message' => 'Berhasil melakukan pengaduan',
            'data' => $this->public_complaint->create($request->validated())
        ], 201);
    }

    public function certificate(CertificateRequest $request)
    {
        $data = $request->validated();
        $data['birth_date'] = Carbon::parse($request->birth_date)->format('Y-m-d');
        return response()->json([
            'status' => true,
            'message' => 'Berhasil melakukan permohonan',
            'data' => $this->certificate->create($data)
        ], 201);
    }

    public function coverLetter(CoverLetterRequest $request)
    {
        $data = $request->validated();
        $data['birth_date'] = Carbon::parse($request->birth_date)->format('Y-m-d');
        $data['valid_from'] = Carbon::parse($request->valid_from)->format('Y-m-d');
        return response()->json([
            'status' => true,
            'message' => 'Berhasil melakukan permohonan',
            'data' => $this->cover_letter->create($data)
        ], 201);
    }

}
