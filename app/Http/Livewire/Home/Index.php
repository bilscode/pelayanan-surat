<?php

namespace App\Http\Livewire\Home;

use App\Models\Certificate;
use App\Models\CoverLetter;
use App\Models\DeathPerson;
use App\Models\PublicComplaint;
use Livewire\Component;

class Index extends Component
{
    public $cover_letters;
    public $certificates;
    public $death_person;
    public $public_complaints;

    public function mount()
    {
        $this->cover_letters = CoverLetter::latest()->get();
        $this->certificates = Certificate::latest()->get();
        $this->death_person = DeathPerson::latest()->get();
        $this->public_complaints = PublicComplaint::latest()->get();
    }

    public function render()
    {
        return view('home.index')
        ->layoutData([
            'title' => 'Dashboard'
        ]);
    }

    public function checkData()
    {
        $cover_letters = CoverLetter::latest()->get();
        $certificates = Certificate::latest()->get();
        $death_person = DeathPerson::latest()->get();
        $public_complaints = PublicComplaint::latest()->get();
        if ($cover_letters->count() > $this->cover_letters->count()) {
            $this->dispatchBrowserEvent('show-notification', [
                'message' => "Terdapat ".$cover_letters->count()-$this->cover_letters->count()." pengajuan surat pengantar baru !",
            ]);
            $this->cover_letters = $cover_letters;
        }
        if ($certificates->count() > $this->certificates->count()) {
            $this->dispatchBrowserEvent('show-notification', [
                'message' => "Terdapat ".$certificates->count()-$this->certificates->count()." pengajuan surat keterangan baru !",
            ]);
            $this->certificates = $certificates;
        }
        if ($death_person->count() > $this->death_person->count()) {
            $this->dispatchBrowserEvent('show-notification', [
                'message' => $death_person->count()-$this->death_person->count()." orang meninggal tercatat !",
            ]);
            $this->death_person = $death_person;
        }
        if ($public_complaints->count() > $this->public_complaints->count()) {
            $this->dispatchBrowserEvent('show-notification', [
                'message' => "Terdapat ".$public_complaints->count()-$this->public_complaints->count()." pengaduan masyarakat baru !",
            ]);
            $this->public_complaints = $public_complaints;
        }
    }
}
