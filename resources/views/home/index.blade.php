<div class="row" wire:poll="checkData">
    <div class="col-md-3">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ $cover_letters->count() }}</h3>
                <p>Surat Pengantar</p>
            </div>
            <div class="icon">
                <i class="fas fa-envelope-open"></i>
            </div>
            <a href="{{ route('mail-monitoring.cover-letter') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
        <div class="card">
            <div class="card-header bg-info">
                <h3 class="card-title">Data Terbaru</h3><br/>
                <small class="text-white mb-0 font-weight-bold" style="font-style: italic;">Surat Pengantar</small>
            </div>
            <div class="card-body">
                @forelse ($cover_letters as $cover_letter)
                    @if ($loop->iteration <= 5)
                    <div class="border-bottom">
                        <p class="font-weight-bold mb-0">{{ $cover_letter->name }}</p>
                        <small class="text-muted mb-0" style="font-style: italic;">{{ $cover_letter->created_at->diffForHumans() }}</small>
                    </div>
                    @endif
                @empty
                    <p class="text-center">Tidak ada data</p>
                @endforelse
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>{{ $certificates->count() }}</h3>
                <p>Surat Keterangan</p>
            </div>
            <div class="icon">
                <i class="fas fa-envelope"></i>
            </div>
            <a href="{{ route('mail-monitoring.certificate') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
        <div class="card">
            <div class="card-header bg-primary">
                <h3 class="card-title">Data Terbaru</h3><br/>
                <small class="text-white mb-0 font-weight-bold" style="font-style: italic;">Surat Keterangan</small>
            </div>
            <div class="card-body">
                @forelse ($certificates as $certificate)
                    @if ($loop->iteration <= 5)
                    <div class="border-bottom">
                        <p class="font-weight-bold mb-0">{{ $certificate->name }}</p>
                        <small class="text-muted mb-0" style="font-style: italic;">{{ $certificate->created_at->diffForHumans() }}</small>
                    </div>
                    @endif
                @empty
                    <p class="text-center">Tidak ada data</p>
                @endforelse
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ $death_person->count() }}</h3>
                <p>Data Orang Meninggal</p>
            </div>
            <div class="icon">
                <i class="fas fa-archway"></i>
            </div>
            <a href="{{ route('death-person') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
        <div class="card">
            <div class="card-header bg-danger">
                <h3 class="card-title">Data Terbaru</h3><br/>
                <small class="text-white mb-0 font-weight-bold" style="font-style: italic;">Data Orang Meninggal</small>
            </div>
            <div class="card-body">
                @forelse ($death_person as $death_people)
                    @if ($loop->iteration <= 5)
                    <div class="border-bottom">
                        <p class="font-weight-bold mb-0">{{ $death_people->name }}</p>
                        <small class="text-muted mb-0" style="font-style: italic;">{{ $death_people->created_at->diffForHumans() }}</small>
                    </div>
                    @endif
                @empty
                    <p class="text-center">Tidak ada data</p>
                @endforelse
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{ $public_complaints->count() }}</h3>
                <p>Pengaduan Masyarakat</p>
            </div>
            <div class="icon">
                <i class="fas fa-file-signature"></i>
            </div>
            <a href="{{ route('public-complaints') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
        </div>
        <div class="card">
            <div class="card-header bg-warning">
                <h3 class="card-title">Data Terbaru</h3><br/>
                <small class="text-dark mb-0 font-weight-bold" style="font-style: italic;">Data Orang Meninggal</small>
            </div>
            <div class="card-body">
                @forelse ($public_complaints as $public_complaint   )
                    @if ($loop->iteration <= 5)
                    <div class="border-bottom">
                        <p class="font-weight-bold mb-0">{{ $public_complaint   ->name }}</p>
                        <small class="text-muted mb-0" style="font-style: italic;">{{ $public_complaint ->created_at->diffForHumans() }}</small>
                    </div>
                    @endif
                @empty
                    <p class="text-center">Tidak ada data</p>
                @endforelse
            </div>
        </div>
    </div>
</div>
