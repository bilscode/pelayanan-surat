<?php

use App\Models\Certificate;
use App\Models\CoverLetter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false
]);

Route::middleware('auth')->group(function () {
    Route::get('/', App\Http\Livewire\Home\Index::class)->name('home');
    Route::name('mail-monitoring.')->prefix('/mail-monitoring')->group(function () {
        Route::get('/cover-letter', App\Http\Livewire\MailMonitoring\CoverLetter\Index::class)->name('cover-letter');
        Route::get('/cover-letter/{id}/pdf', function ($id, CoverLetter $coverLetter) {
            $coverLetter = $coverLetter->findOrFail($id)->toArray();
            $pdf = Pdf::loadView('mail-monitoring.cover-letter.pdf', [
                'cover_letter' => $coverLetter
            ]);
            return $pdf->stream('surat-pengantar-'.Str::slug($coverLetter['name'], '-').'-'.now()->timestamp.'.pdf');
        })->name('cover-letter.pdf');
        Route::get('/certificate', App\Http\Livewire\MailMonitoring\Certificate\Index::class)->name('certificate');
        Route::get('/certificate/{id}/pdf', function ($id, Certificate $certificate) {
            $certificate = $certificate->findOrFail($id)->toArray();
            $pdf = Pdf::loadView('mail-monitoring.certificate.pdf', [
                'certificate' => $certificate
            ]);
            return $pdf->stream('surat-pengantar-'.Str::slug($certificate['name'], '-').'-'.now()->timestamp.'.pdf');
        })->name('certificate.pdf');
    });
    Route::get('/death-person', App\Http\Livewire\DeathPerson\Index::class)->name('death-person');
    Route::get('/public-complaints', App\Http\Livewire\PublicComplaint\Index::class)->name('public-complaints');
});
